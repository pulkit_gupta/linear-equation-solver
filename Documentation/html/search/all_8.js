var searchData=
[
  ['main',['main',['../d5/d7c/class_swing_g_u_i.html#a3fd8ecc3f5c02a9e49e103448ec36b28',1,'SwingGUI']]],
  ['mainpage_2edox',['mainPage.dox',['../dd/d03/main_page_8dox.html',1,'']]],
  ['matrix',['Matrix',['../d6/d3f/class_matrix.html',1,'Matrix'],['../d6/d3f/class_matrix.html#a9b1f3e8d929e0bbfdd9be3e58bd4e28c',1,'Matrix.Matrix(double[][] dat)'],['../d6/d3f/class_matrix.html#a1f5d211829fd18cfd43668113914cfab',1,'Matrix.Matrix(int nrow, int ncol)'],['../d5/d7c/class_swing_g_u_i.html#acda3039404ebdb35cca6652e04ca6567',1,'SwingGUI.matrix()']]],
  ['matrix_2ejava',['Matrix.java',['../d0/dcd/_matrix_8java.html',1,'']]],
  ['matrixinput',['matrixInput',['../d5/d7c/class_swing_g_u_i.html#a8063d72f46142b8fd79c5ecae7f4f8b5',1,'SwingGUI']]],
  ['multiplytovector',['multiplyToVector',['../d6/d3f/class_matrix.html#ae17fa4138493c0e0e0586f39615082b2',1,'Matrix']]]
];
