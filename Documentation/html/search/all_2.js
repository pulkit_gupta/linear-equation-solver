var searchData=
[
  ['data',['data',['../d6/d3f/class_matrix.html#a7b84f960b65658df6df942d443d7524e',1,'Matrix.data()'],['../d5/db2/class_vector.html#adc375ae0f0780cb0894f6b9211b7725c',1,'Vector.data()']]],
  ['determinant',['determinant',['../d6/d8d/class_solver.html#a9e0c65ba79d5abc12ff989d6afa5b805',1,'Solver']]],
  ['distribution',['Distribution',['../d5/d57/extra_8dox.html#aab647d568741aa0e7800856acd1c306b',1,'extra.dox']]],
  ['divideby',['divideBy',['../d5/db2/class_vector.html#ad546510c2e8189a03652066333357764',1,'Vector']]],
  ['dividebyconstant',['divideByConstant',['../d6/d3f/class_matrix.html#a37087ca099fd0fb3e407cb93a271bbc7',1,'Matrix']]]
];
