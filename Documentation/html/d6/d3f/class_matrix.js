var class_matrix =
[
    [ "Matrix", "d6/d3f/class_matrix.html#a9b1f3e8d929e0bbfdd9be3e58bd4e28c", null ],
    [ "Matrix", "d6/d3f/class_matrix.html#a1f5d211829fd18cfd43668113914cfab", null ],
    [ "divideByConstant", "d6/d3f/class_matrix.html#a37087ca099fd0fb3e407cb93a271bbc7", null ],
    [ "getNcols", "d6/d3f/class_matrix.html#ad0a370be969465db311a9442f6ee03f8", null ],
    [ "getNrows", "d6/d3f/class_matrix.html#ac7db3a899e2fd0c6ed0536a2ace7e733", null ],
    [ "getValueAt", "d6/d3f/class_matrix.html#adf1b2d3ad124657e7da61714bbbf595a", null ],
    [ "isSquare", "d6/d3f/class_matrix.html#ada128a8626e633c713576ecbbd28a153", null ],
    [ "multiplyToVector", "d6/d3f/class_matrix.html#ae17fa4138493c0e0e0586f39615082b2", null ],
    [ "printMat", "d6/d3f/class_matrix.html#a7c92c497c75743c19336f6f53ed71935", null ],
    [ "setValueAt", "d6/d3f/class_matrix.html#a2ac972c7104d2b3821431ef0f90e7db6", null ],
    [ "size", "d6/d3f/class_matrix.html#a946333d7c5a39a4b2d5861c25e3ec82c", null ],
    [ "toVector", "d6/d3f/class_matrix.html#a1df33017b5d0d2b49861e69edd0083ed", null ],
    [ "data", "d6/d3f/class_matrix.html#a7b84f960b65658df6df942d443d7524e", null ],
    [ "ncols", "d6/d3f/class_matrix.html#ae5de537411506dfdf5124a7f5376f7ab", null ],
    [ "nrows", "d6/d3f/class_matrix.html#aa44273ea79fa386a50b9c267db3c4423", null ]
];