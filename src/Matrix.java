/**
* @author: Pulkit Gupta
* Class has only three private members for managing data.
* It handles most functions required related to Matrix.
*/
class Matrix { 

	private int nrows; 
	private int ncols; 
	private double[][] data;

	/**
	 * Setter function for the class.
	 * @param x is the row location
	 * @param y is the column location
	 * @param value is the double value to set
	 * @return true if x and y arguments does not exceed the size of data present itself.
	 */
	public boolean setValueAt(int x, int y, double value){
		if(x> nrows || y> ncols){
			return false;
		}
		data[x][y] = value;
		return true;
	}

	/**
	 * Getter Function for the class
	 * @param x is row location
	 * @param y is column location
	 * @return double value of that particular index
	 */
	public double getValueAt(int x, int y){
		if(x> nrows || y> ncols){
			//through exception
		}
		return data[x][y];
	}

	/**
	* @return comparision of columns and rows of the matrix
	*/
	public boolean isSquare (){
		return nrows==ncols;
	}

	/**
	* Getter function to get the column size.
	* @return: Provide the number of columns of the matrix
	*/
	public int getNcols (){
		return ncols;
	}

	/**
	* Provide the number of  rows of the Matrix
	* @return: rows count 
	*/
	public int getNrows (){
		return nrows;
	}

	/**
	* @function: Provide the size of the Matrix (square matrix) 
	*/
	public int size(){
		return nrows;
	}	

	/**
	*	Constructor to input the value of matrix in form of double 2D array which is passed by value so that there is no access to the variable once its gone to class scope variable.
	*	@param double[][] dat : a double 2 Dimensional array is passed to constructor
	*	
	*/
	public Matrix(double[][] dat) {
		
		this.nrows = dat.length; 
		this.ncols = dat[0].length; 
		this.data = new double[nrows][ncols];
		for (int i=0; i<nrows ;i++ ) {
			for (int j =0; j<ncols ;j++ ) {
				this.data[i][j] = dat[i][j];		
			}
		}				
	} 
	/**
	 * Overload of the class constructor to get empty matrix with nrow and n column
	 * @param nrow
	 * @param ncol
	 */
	public Matrix(int nrow, int ncol) { 
		this.nrows = nrow; this.ncols = ncol; 
		data = new double[nrow][ncol]; 
	}

	/**
	 * Divide the matrix object's data by the argument (Scalar division).
	 * @param divider
	 * @return Matrix object after operation
	 */
	public Matrix divideByConstant(double divider){		
		Matrix mat = new Matrix(this.getNrows(), this.getNcols()); 
		for (int i=0;i<this.getNrows();i++){ 
			for (int j=0; j<this.getNcols();j++){ 
				mat.setValueAt(i,j,this.getValueAt(i,j)/ divider);				
			} 
		} 
		return mat; 
	}
	

	/**
	 * Matrix to Vertical vector multiplication
	 * @param vec
	 * @return Vector object after operation
	 */
	public Vector multiplyToVector(Vector vec){

		if(vec.getLength() == this.getNcols()) {			
			Matrix mat = new Matrix(1, this.getNrows()); 
			double sum =0;
			for (int i=0;i<this.getNrows();i++){ 
				sum = 0;				
				for (int j=0; j<this.getNcols();j++){ 
					sum += vec.getValueAt(j)*this.getValueAt(i,j);					
				} 				
				mat.setValueAt(0,i,sum);				
			} 
			return mat.toVector(0); 		
		}

		else{
			/**
			*	can also raise an exception in future
			*/			
			return null;
		}
	}

	/**
	* Convert the Matrix data-object into Vector. Validation should be made before calling this function.
	* @param index: value of row which is to be converted to vector
	* @return: Vector object
	*/
	public Vector toVector(int index){
		return new Vector(data[index]);
	}

	/**
	 * Prints the Matrix in memory. Usually used for the printing of matrix while debugging
	 */
	public void printMat(){
		for (int i =0;i <nrows ;i++ ) {
			System.out.print("\n");
			for (int j =0; j<ncols;j++ ) {
				System.out.print(String.valueOf(data[i][j])+" ");
			}
		}
		System.out.print("\n");
	}
}