
/**
 * \mainpage Linear Equation System (GUI) V 0.1
 * \section intro_sec Introduction
 * 
 * System is build in Java platform using swing (For GUI) and JOGL2.0 (for Visualization). System works on small Library for linear system solver which is based custom data structures like Matrix and Vector wrapper class.

In all System is divided into 3 Parts (Analogous to class)

1. GUI with swing (2D Design UI): It has all the design blocks and Getter and setter functions to updat UI.

2. Solver System  (Algorithm): It basically works on algorithm using Custom Data Structures/Class.

3. Visualization (3D): Using JOGL2.0 (Java for OpenGL) A small visualization sample is also designed where Cubes and Pyramids movements can be controlled by input data to this System using API.
 * 
 * 
 * 
 * \section algo_sec Algorithmic detail
 * 
 * The complete API for solving linear system is done by Matrix system (Mainly Cramer's Rule algorithm) where determinant. Cofactors, Adjoints and operations like mulitplications are done to solve given linear equation system.
API is designed in such a way that it can be used outside for simple operation by local users and also (Developer) as internal whole integrated system. 

for example: you can call the matrix multiply function to multiply operation in main function directly  and also is used by inside by other function. Although function can access the value from class scope variable, all Functions are coded with arguments Matrix given in signature. 

This enables the functional definition works independently outside the class too. 
 * 
 * 
 * 
 * 
 * \section dep_sec Dependencies/Setup
 * 
 * 
 * As this system is also including 3D drawing, some Libraries apart from the native ones are required. This time it is OpenGL (JOGL2.0)
OpenGL is a cross-platform, language-independent, industrial standard API for producing 3D (and 2D) computer graphics. Graphics cards that claim OpenGL-compliance make use of the hardware acceleration when possible to speed up the graphics rendering process.OpenGL competes with Direct3D on Microsoft Windows platform. The OpenGL mother site is at http://www.opengl.org.
 * 
 * 
 * 
 * \section install_sec Steps for operation (For end-user)
 * 
 * 
 * \subsection step1 Step 1: Installing JRE
 *
 *First setup would be to install JRE (Java Runtime environment) tested for version 1.5 or later. You can download and install JRE from http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html depending upon your Operating system version and type.

There is no need of JDK (Java Development Kit) until, you want to recompile the Software from source code. In that case you would also need some more libraries (.jar files) which is also put inside rootFolder/"Library" folder.
 *
 *
 *	\subsection step2 Step 2: Finding Executable for your platform (OS)
 *	
 *	This software has ability to run on any platform. To execute this software, please go to the folder [rootFolder]/bin/platforms-dep/ [Platform-name] / LinearEquationSystem.jar

For example: If you are using Windows 32 Bit, Path would be:
	[rootFolder]/bin/platforms-dep/windows-i586/ LinearEquationSystem.jar

	If you are using Window-64 bit, Path would be:
	[rootFolder]/bin/platforms-dep/windows-amd64/ LinearEquationSystem.jar

	In case, you can not run jar file on your system by double click, Please use following command given below:
	1. Open command prompt (in windows) or terminal in Linux.
	2. Using CD command, go to the respective location where .jar file is located (corresponding to your OS).
	3. run command java -jar LinearEquationSystem.jar
	4. Software will run
	
	Similarly, Any Operating system can be chosen.
 *
 *
 *	\subsection step3 Step 3: Operation
 *	
 *	GUI is very user friendly where all validations are implemented for the user input. For an instance- 

1. Coefficient Matrix should be square.
2. Coefficient Matrix's length should be equal to length of Right side constant vectors.
3. There should not be any special character other than . and -
4. There should not be any alphabet or alphanumeric number in any of the input.

System will detect and show user "Warning message".In project folder, you can also find screen shots of working software in rootFolder.
 *
 *
 *
 *	\section developers_sec Further Development/Source Code Distribution (For Developers)
 *	
 *	As the library used in this software is developed in such a way that it can be used for any further development. One can copy Matrix, Vector and Solver java class and use them just by one object refference to Solver class. There are several constructors overload implemented in all of these classes.
With this, There is a need of JDK (Java Development Kit) on which these libraries are based. Source code is also available on Git repository.

git clone https://bitbucket.org/pulkit_gupta/linear-equation-solver/

For compiling whole software and extending it, you would also need some more libraries (.jar files) which is also put inside rootFolder/"Library" folder.

The complete development is done in Eclipse Kepler service release-1. It can be resumed any time with the same IDE. Project files are also available in root folder of the whole project.

Source code is divided into 5 Java classes.

1. Matrix.java
2. Vector.java
3. Solver.java
4. SwingGUI.java
5. OpenGLGUI.java

Every class is heavily commented and documentation of all blocks of code can be found by using navigation bar on the left.
Currently documentation of OpenGLGUI.java is not generated to avoid complexity. But code can be accessed through Navigation Panel.
 *	
 *
 *	\section feedback_sec Feedback and Query
 *	
 *	
 *	For any query regarding the malfunctioning of the software, please contact developer at the address below:

Pulkit Gupta

Email: pulkit.itp@gmail.com
 *	
 *
 *
 *
 *
 *
 */
