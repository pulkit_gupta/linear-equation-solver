/**
*
*/

public class Solver {	

	private Matrix weights;
	private Vector constants;
	private Vector result;
	private boolean calculated;
	private boolean isValid;

	public Solver(double data[][], double v[]){			

		calculated = false; 
		result = null;

		this.weights = new Matrix(data);
		this.constants = new Vector(v);

		updateSystem();
		
	}

	public boolean isCalculated(){
		return calculated;
	}

	public boolean updateSystem(){		

		System.out.println("Values are "+ weights.size() + " ->"+ constants.getLength() +" -> "+ weights.isSquare());
		if((weights.size() == constants.getLength()) && (weights.isSquare())){
			isValid = true;	
			System.out.println("System is Valid");
		}
		else{
			isValid = false;
		}
		return isValid;
	}
	public boolean setWeights(double data[][]){

		this.weights = new Matrix(data);
		updateSystem();
		return isValid;
	}

	public Matrix getWeights(){
		return weights;		
	}

	public boolean setConstants(double v[]){		
		this.constants = new Vector(v);
		updateSystem();
		return isValid;
	}

	public Vector getConstants(){
		return constants;
	}

	/**
	*	Can also through exception in case function called on invalid data
	*/
	public Vector solve(){
		if(isValid){
			System.out.println("Everythings okay and calculating");			
			Vector answer = inverse(weights).multiplyToVector(constants).divideBy(determinant(weights));			
			if(answer !=null){				
				calculated = true;
				this.result = answer; //to class scope for later access
				System.out.println("returning answers is"+ this.result);				
				return answer;	
			}
			else{
				calculated = false;
				return null;
			}		
		}
		else{
			return null;  
		}
			
	}

	public int changeSign (int val){	
		return val%2==0?1:-1;
	}

	public Matrix createSubMatrix(Matrix matrix, int excluding_row, int excluding_col){ 
		Matrix mat = new Matrix(matrix.getNrows()-1, matrix.getNcols()-1); 
		int r = -1; 
		int c = -1;
		for (int i=0;i<matrix.getNrows();i++){ 
			if (i==excluding_row){
				continue; 
			}
			r++; 
			c = -1;
			for (int j=0;j<matrix.getNcols();j++){ 
				if (j==excluding_col){
					continue; 
				}	
				mat.setValueAt(r, ++c, matrix.getValueAt(i, j)); 
			} 
		} 
			return mat; 
	}

	public Matrix transpose(Matrix matrix){ 
		Matrix transposedMatrix = new Matrix(matrix.getNcols(), matrix.getNrows());
		for (int i=0;i<matrix.getNrows();i++){
		 	for (int j=0;j<matrix.getNcols();j++){
		 	 	transposedMatrix.setValueAt(j, i, matrix.getValueAt(i, j)); 
		 	 } 
		} 
		return transposedMatrix; 
	}
	
	public Matrix cofactor(Matrix matrix){ 
		Matrix mat = new Matrix(matrix.getNrows(), matrix.getNcols()); 
		for (int i=0;i<matrix.getNrows();i++){ 
			for (int j=0; j<matrix.getNcols();j++){ 
				double value = determinant(createSubMatrix(matrix, i, j));
				//System.out.println("making cofactors = "+ value);
				value = value * changeSign(i) * changeSign(j) ;
				//System.out.println("making cofactors after sign change = "+ value);
				mat.setValueAt(i, j, value); 
			} 
		} 
		return mat; 
	}
		
	public double determinant(Matrix matrix){
		if (!matrix.isSquare()){
			/**
			*	if no equal row and column
			*/
			return -1;
		} 	
     	else if (matrix.size()==2){ 
     		return (matrix.getValueAt(0, 0) * matrix.getValueAt(1, 1)) - ( matrix.getValueAt(0, 1) * matrix.getValueAt(1, 0)); 
     	} 
     	else if(matrix.size()==1){
     		return matrix.getValueAt(0,0);
     	}
     	double sum = 0.0; 
     	for (int i=0; i<matrix.getNcols(); i++){
     	 	sum += changeSign(i) * matrix.getValueAt(0, i) * determinant(createSubMatrix(matrix, 0, i)); 
     	} 
     	return sum; 
    }

    public Matrix inverse(Matrix matrix) 
    { 
     	Matrix cofactorm = cofactor(matrix);
     	System.out.println("Cofactor calculated is ");
     	cofactorm.printMat();
     	Matrix transposem = transpose(cofactorm);
     	System.out.println("transpose of Cofactor calculated is ");
     	transposem.printMat();
     	/*double determinantm = determinant(matrix);
     		System.out.println("determinant of materix is"+ determinantm);
    		double doubleval = (double)1.0/determinantm;
     		System.out.println("final reciprocal of determinant of materix is"+ doubleval);
     	*/
     		//divide later because of round off problem
    	return transposem;//.divideByConstant(determinantm); 
    }

}