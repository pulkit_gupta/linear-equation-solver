/**
 * Class Vector is a wrapper class for the Vector data.
 * It also handle some of the operations related to Vector.
 * @author Pulkit Gupta
 *
 */
class Vector{
	private double data[];
	private int size;

	/**
	 * 
	 * @param len takes the length off empty Vector
	 */
	public Vector(int len){
		size = len;		
		data = new double[len];
	}
	/**
	 * Constructor Overload
	 * It initialize the data which arr (by value)
	 * @param arr 
	 */
	public Vector(double arr[]){
		size = arr.length;		
		data = new double[size];
		for (int i =0; i< size; i++) {
			data[i] = arr[i];
		}				
	}
	
	/**
	 * Setter function to change the value at index i with val 
	 * @param i is index.
	 * @param val
	 */
	public void setDataAt(int i, double val){
		data[i] = val;
	}
	/**
	 * Getter function to get the value at particular index.
	 * @param i
	 * @return double value of the particular location i.
	 */
	public double getValueAt(int i){
		if(i>size){
			//or can throw exception in future
		}
		return data[i];
	}
	/**
	 * 
	 * @return length of the vector stored in the calling object.
	 */
	public int getLength(){
		//System.out.println("value sent is "+ size);
		return size;
	}


	/**
	 * divides the vector (scaler) by the value in argument.
	 * @param value
	 * @return vector object after operation.
	 */
	public Vector divideBy(double value){
		if(value==0){
			System.out.println("Value of determinant is 0");
			return null;
		}
		Vector result = new Vector(this.size);
		for (int i=0; i<size ;i++ ) {
			result.setDataAt(i, this.data[i] / value);
		}
		return result;
	}

	/**
	 *  Prints the value stored inside the Class data variable. (Mainly during debugging)
	 */
	public void print(){
		for (int i=0;i< size ;i++ ) {
			System.out.print(data[i]+ ", " );			
			//System.out.print("x"+(i+1)+" = "+ data[i]+ "\n" );			
		}
	}
	
	/**
	 *	Convert vector into String array with some prefix. 
	 * @return array of strings where each element is the value of variables of lineare system
	 */
	public String[] formattedOutput(){
		String str[] = new String[size];
		for(int i =0;i<size;i++){
			str[i] = "x"+String.valueOf(i)+" = "+String.valueOf(data[i]); 
		}
		return str;
	}
	/**
	*	Setters are to be implemented in future if needed.
	*/
	public void setValue(int i, double data){
		
	}

}

