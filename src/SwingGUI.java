/**
 * Implementation related to swing GUI
 * Accessing custom classed like Matrix, Vector, Solver, OpenGLGUI
 * @author Pulkit Gupta
 * 
 */

import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;

/**
 *	Class to implement GUI related to swing
 * 
 * @author Pulkit
 *
 */
public class SwingGUI implements ActionListener{

	/**
	 *  Variables for final output to screen
	 */
	private JFrame frame;
	private JTextField constant;
	private JTextArea matrix;
	private JButton submit;
	private JTextArea outputValue;
	private Canvas canvas;

	/**
	 *  Variables to relate with class Solver
	 */
	private double[][] matrixInput;
	private double[] constantsInput;


	/**
	 *  Start the process of calculation to visualization
	 *  It is not inheriting JFrame instead using the Object as class scope member.
	 *  @return void
	 */
	public void runProcess(){
		
		String text[] = {"Invalid DATAsets"};
		if(fetchMatrix() ==null || fetchVector()==null ){
			outputValue.setText("Invalid inputs. \nCharacters not alowed");
			
			((OpenGLGUI) canvas).setMsgString(text);
			((OpenGLGUI) canvas).startAnimation(false);
		}
		else{
			Vector v = new Solver(matrixInput, constantsInput).solve();
			if(v!=null){
				String arr[] = v.formattedOutput();
				((OpenGLGUI) canvas).setMsgString(arr);
				((OpenGLGUI) canvas).startAnimation(true);
				System.out.println("FINAL is"+ arr.toString());
				String combined = "";
				for(int i=0; i<arr.length; i++){
					combined = combined + arr[i]+ " ;\n";
				}
				outputValue.setText(combined);				
			}
			else
			{
				outputValue.setText("Can not solve this equation. \n\nReason Hints:\n\nEither input data dimensions are not valid.\nOr Determinant of matrix = 0");
				((OpenGLGUI) canvas).setMsgString(text);
				((OpenGLGUI) canvas).startAnimation(false);
			}
		}


	}

	/**
	 * Fetch the matrix from the string by doing a lot of string processing mainly by using the StringTokenizer.
	 *   
	 * @return double 2D array which inturn used to initialize Solver class object.
	 */

	public double[][] fetchMatrix(){

		try{
			String str = matrix.getText();
			StringTokenizer stRows = new StringTokenizer(str,"\n");
			int rows = stRows.countTokens();
			System.out.println("rowss are"+ rows);
			double[][] x = new double[rows][rows];  //as it is a square matrix

			int i=0, j=0;  //indexes to use

			while(stRows.hasMoreTokens()){
				StringTokenizer stCols = new StringTokenizer(stRows.nextToken(),";");
				j=0;
				while(stCols.hasMoreTokens()){
					x[i][j] = Double.parseDouble(stCols.nextToken());
					//System.out.println("value of i="+i +" and j="+j+" is "+x[i][j]);
					j++;
				}

				i++;
			}
		 	Matrix m = new Matrix(x);
			m.printMat();  //only to check
			this.matrixInput = x;  //saving at class scope
			return x;
		}
		/**
		 *  Catch all the exceptions but the main aim is to capture NumberFormatException
		 */
		catch(Exception e){
			/**
			 *  Again in future exception based error tracing will be done.
			 */
			return null;  //tells the user about invalid inputs
		}

	}

	/**
	 *  Calculate the 1D double array again by doing String processing
	 * @return double 1D array containing the vector
	 */
	public double[] fetchVector(){
		double x[] = null;
		try{
			StringTokenizer stCols = new StringTokenizer(constant.getText(),";");
			int columns = stCols.countTokens();
			System.out.println("column value for constant's vector is "+columns);
			x= new double[columns];
			int j=0;
			while(stCols.hasMoreTokens()){
				x[j] = Double.parseDouble(stCols.nextToken());
				//System.out.println("value of i="+i +" and j="+j+" is "+x[i][j]);
				j++;
			}

		}
		catch(Exception e){
			return null;  //catching NumberFormatException and hence returning null.
		}
		constantsInput = x; //saving reference to class scop variable
		return x;
	}

	/**
	 *  Constructor to start and prepare GUI with swing
	 *  
	 */
	public SwingGUI(){

		/**
		 *  Initializing class scope variable which will hold data and controls that will perform action
		 */
		String defaultData[] = {"No DATA"};
		canvas =  OpenGLGUI.launch(defaultData);
		((OpenGLGUI) canvas).startAnimation(false);
		frame = new JFrame("User interaction panel");
		constant = new JTextField("1;2;3;3;45;6;1;2;3", 15);
		matrix = new JTextArea("1;2;3;2;1;2;1;2;4\n4;5;6;2;3;4;4;5;8\n7;81;9;1;23;43;7;8;5\n4;5;6;1;3;4;2;3;1\n7;81;9;2;4;5;1;2;4\n7;81;91;3;4;5;9;8;7\n7;81;91;3;4;25;9;8;7\n4;5;6;22;3;4;4;5;8\n1;2;3;4;5;6;7;8;9\n", 15, 25);
		submit = new JButton("submit");

		/**
		 * 	Designing of the GUI first panel
		 */
		JLabel label1 = new JLabel("Enter the Coefficient in the Box (eg- 1;2;3;4 nextline for next row)");
		JPanel firstPanel = new JPanel(new FlowLayout());
		firstPanel.add(label1);
		firstPanel.add(matrix);

		/**
		 * 	Designing of midPanel GUI
		 */


		JLabel label2 = new JLabel("Constants vector:");
		JPanel midPanel = new JPanel(new FlowLayout());
		midPanel.add(label2);
		midPanel.add(constant);
		midPanel.add(submit);

		/**
		 * 	Designing of Last panel of Grid Layout
		 */
		JLabel label3 = new JLabel("Output:");
		outputValue = new JTextArea("     ", 17,30);
		//outputValue.setEditable(false);
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		outputValue.setBorder(BorderFactory.createCompoundBorder(border,
		            BorderFactory.createEmptyBorder(10, 10, 10, 10)));


		JPanel lastPanel = new JPanel(new FlowLayout());

		lastPanel.add(label3);
		lastPanel.add(outputValue);

		/**
		 * 	Adding all Panels to the frame window
		 */
		firstPanel.add(midPanel);
		frame.add(firstPanel);
		frame.add(lastPanel);

		//frame.add(midPanel);
		GridLayout gridLayout = new GridLayout(2,1);
		gridLayout.setVgap(1);
		frame.setLayout(gridLayout);

		/**
		 *  Setting up the Frame variables
		 */
		frame.addWindowListener(new WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        if (JOptionPane.showConfirmDialog(frame,
		            "Are you sure to close all windows?", "Really Closing?",
		            JOptionPane.YES_NO_OPTION,
		            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		            	System.exit(0);
		        }
		        else{
		        	System.out.println("pressed No");		        	
		        	frame.setVisible(true);
		        }
		    }
		});

		submit.addActionListener(this);
		frame.setResizable(false);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);	    
		frame.setLocation(x, 10);
		frame.setSize(400, 700);
		frame.setVisible(true);
	}

	/**
	 * Main entry point of the Program just run the constructor of SwingGUI class
	 * 
	 * @param str
	 */
	public static void main(String str[]){
		new SwingGUI();				
	}
	/**
	 * @param Actionevent object which is fed automatically by JButton addListener
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equals("submit")) {
			//String message = "Button1 has been clicked and value of both fields are"+ matrix.getText()+ " and "+ constant.getText();
			//JOptionPane.showMessageDialog(frame, message);
			//System.out.println(message);
			runProcess();

		}

	}
}
