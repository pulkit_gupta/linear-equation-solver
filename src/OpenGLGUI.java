
/**
 * Imports including java swing and JOGL
 * JOGL is java for openGL which is the biggest wrapper of C++ native code in Java via JNI.
 * @author Pulkit Gupta
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.awt.TextRenderer;

import static javax.media.opengl.GL.*;  // GL constants
import static javax.media.opengl.GL2.*; // GL2 constants
 

@SuppressWarnings("serial")
/**
 * Java OpenGL class
 *  It inherts the GLCanvasfor drawing  and implements GLListener event for animation 
 */
public class OpenGLGUI extends GLCanvas implements GLEventListener {
   // Define constants for the top-level container
   private static String TITLE = "Sample OpenGL Data driven 3D drawing.";  // window's title
   private static final int CANVAS_WIDTH = 620;  // width of the drawable
   private static final int CANVAS_HEIGHT = 340; // height of the drawable
   private static final int FPS = 90; // animator's target frames per second
   private static  FPSAnimator animator;
   private TextRenderer textRenderer;
   private static float rotateAngle = 0.0f;
   private String msg[] = {"NO DATA"};
   private int index_for_msg = 0;   

   /**
    * The entry static launch() method to setup the top-level container and animator 
    * @param args have currently no use.
    * 
    * @return Canvas object which is superclass of this class OpenGLGUI.
    */
   
   public static Canvas launch(String[] args) {
      // Run the GUI codes in the event-dispatching thread for thread safety	           
      // Create the OpesnGL rendering canvas
      GLCanvas canvas = new OpenGLGUI();
      //String paramTest[] = {"hello1","second"};             
      ((OpenGLGUI) canvas).setMsgString(args);
      canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
     // Create a animator that drives canvas' display() at the specified FPS.
      animator = new FPSAnimator(canvas, FPS, true);
     // Create the top-level container
     final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
     frame.getContentPane().add(canvas);
     frame.addWindowListener(new WindowAdapter() {
    	 @Override
    	 public void windowClosing(WindowEvent e) {
    		 // Use a dedicate thread to run the stop() to ensure that the
    		 // animator stops before program exits.
    		 new Thread() {
    			 @Override
    			 public void run() {
    				 if (animator.isStarted()) animator.stop();
    				 System.exit(0);
    			 }
    		 }.start();
      		}
      	});
     frame.setTitle(TITLE);
     frame.pack();
     frame.setVisible(true);
     animator.start(); // start the animation loop    
     return canvas;
   }
 

// Setup OpenGL Graphics Renderer
   private GLU glu;  // for the GL Utility
   private float anglePyramid = 0;    // rotational angle in degree for pyramid
   private float angleCube = 0;       // rotational angle in degree for cube
   private float speedPyramid = 2.0f; // rotational speed for pyramid
   private float speedCube = -1.5f;   // rotational speed for cube

   /**
    * Constructor to setup the GUI for this Component
    */
   public OpenGLGUI() {
      this.addGLEventListener(this);
   }
   
   /**
    *  Constructor overload to get object of OpenGLGUI with default text message string arary
    *  @param arg is string array to initialize the message display for the 3D canvas.
    */
   public OpenGLGUI(String[] arg){
	   this.msg = arg;
	   this.addGLEventListener(this);
   }
   
   /**
    * Provides access from outside the class to start or stop rotation
    * @param val
    * @return value true if animator is switched on and false if Off. 
    */
   public boolean startAnimation(boolean val){
	   if(val)
		   	animator.start();
	   else
		   animator.stop();
	   // everything goes well till here.
	   return val;
   }
   
   /**
    * Setter for OpenGLGUI
    * @param arg is string array to set and change the message and data to OpenGL screen on run time.
    */
   public void setMsgString(String arg[]){
	   this.msg = arg; //at present copy by address is fine.
	   index_for_msg = 0;
   }
   
   /**
    * Resets the index of the sequence of solutions display.
    */
   public void reset(){
	   index_for_msg =0;
   }
   // ------ Implement methods declared in GLEventListener ------
 
   /**
    * Called back immediately after the OpenGL context is initialized. Can be used
    * to perform one-time initialization. Run only once.
    * @param: drawable object to draw and get gl object.
    */
   @Override
   public void init(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();      // get the OpenGL graphics context
      glu = new GLU();                         // get GL Utilities
      gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // set background (clear) color
      gl.glClearDepth(1.0f);      // set clear depth value to farthest
      gl.glEnable(GL_DEPTH_TEST); // enables depth testing
      gl.glDepthFunc(GL_LEQUAL);  // the type of depth test to do
      gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // best perspective correction
      gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out lighting

      textRenderer = new TextRenderer(new Font("SansSerif", Font.BOLD, 12));
   }
 
   /**
    * Call-back handler for window re-size event. Also called when the drawable is
    * first set to visible.
    */
   @Override
   public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
 
      if (height == 0) height = 1;   // prevent divide by zero
      float aspect = (float)width / height;
 
      // Set the view port (display area) to cover the entire window
      gl.glViewport(0, 0, width, height);
 
      // Setup perspective projection, with aspect ratio matches viewport
      gl.glMatrixMode(GL_PROJECTION);  // choose projection matrix
      gl.glLoadIdentity();             // reset projection matrix
      glu.gluPerspective(45.0, aspect, 0.1, 100.0); // fovy, aspect, zNear, zFar
 
      // Enable the model-view transform
      gl.glMatrixMode(GL_MODELVIEW);
      gl.glLoadIdentity(); // reset
   }
 
   /**
    * Called back by the animator to perform rendering.
    * @param drawable object is passed and used for drawing
    */
   @Override
   public void display(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
      gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear color and depth buffers
 
      // ----- Render the Pyramid -----
      gl.glLoadIdentity();                 // reset the model-view matrix
      gl.glTranslatef(-1.6f, 0.0f, -6.0f); // translate left and into the screen
      gl.glRotatef(anglePyramid, -0.2f, 1.0f, 0.0f); // rotate about the y-axis
 
      gl.glBegin(GL_TRIANGLES); // of the pyramid
 
      // Font-face triangle
      gl.glColor3f(1.0f, 0.0f, 0.0f); // Red
      gl.glVertex3f(0.0f, 1.0f, 0.0f);
      gl.glColor3f(0.0f, 1.0f, 0.0f); // Green
      gl.glVertex3f(-1.0f, -1.0f, 1.0f);
      gl.glColor3f(0.0f, 0.0f, 1.0f); // Blue
      gl.glVertex3f(1.0f, -1.0f, 1.0f);
 
      // Right-face triangle
      gl.glColor3f(1.0f, 0.0f, 0.0f); // Red
      gl.glVertex3f(0.0f, 1.0f, 0.0f);
      gl.glColor3f(0.0f, 0.0f, 1.0f); // Blue
      gl.glVertex3f(1.0f, -1.0f, 1.0f);
      gl.glColor3f(0.0f, 1.0f, 0.0f); // Green
      gl.glVertex3f(1.0f, -1.0f, -1.0f);
 
      // Back-face triangle
      gl.glColor3f(1.0f, 0.0f, 0.0f); // Red
      gl.glVertex3f(0.0f, 1.0f, 0.0f);
      gl.glColor3f(0.0f, 1.0f, 0.0f); // Green
      gl.glVertex3f(1.0f, -1.0f, -1.0f);
      gl.glColor3f(0.0f, 0.0f, 1.0f); // Blue
      gl.glVertex3f(-1.0f, -1.0f, -1.0f);
 
      // Left-face triangle
      gl.glColor3f(1.0f, 0.0f, 0.0f); // Red
      gl.glVertex3f(0.0f, 1.0f, 0.0f);
      gl.glColor3f(0.0f, 0.0f, 1.0f); // Blue
      gl.glVertex3f(-1.0f, -1.0f, -1.0f);
      gl.glColor3f(0.0f, 1.0f, 0.0f); // Green
      gl.glVertex3f(-1.0f, -1.0f, 1.0f);
 
      gl.glEnd(); // of the pyramid
 
      // ----- Render the Color Cube -----
      gl.glLoadIdentity();                // reset the current model-view matrix
      textRenderer.begin3DRendering();

      gl.glTranslatef(-5.0f, 15.0f, -50.0f);
      
      /**
       *  Rotate around X axis
       */
      //gl.glRotatef(rotateAngle, 1.0f, 0.0f, 0.0f);       
      // other angles
      //gl.glRotatef(rotateAngle * 1.5f, 0.0f, 1.0f, 0.0f);
      //gl.glRotatef(rotateAngle * 1.4f, 0.0f, 0.0f, 1.0f);

      // uncomment for Pulsing Colors Based On Text Position
      
      /*
          textRenderer.setColor((float)(Math.cos(rotateAngle / 20.0f)),      // R
            (float)(Math.sin(rotateAngle / 25.0f)),                      // G
            1.0f - 0.5f * (float)(Math.cos(rotateAngle / 17.0f)), 0.5f); // B
       */
      textRenderer.setColor(1.0f,1.0f,1.0f, 1.0f);
      // String, x, y, z, and scaling - need to scale down
      // Not too sure how to compute the x, y and scaling - trial and error!
      textRenderer.draw3D(msg[index_for_msg], -20.0f,
            0.0f, 0.0f, 0.4f);
      // Clean up rendering
      textRenderer.end3DRendering();

      // Update the rotate angle
      //rotateAngle += 0.9f;
      rotateAngle -= speedCube/2;
      if(rotateAngle%100 >= 99){   
    	//animator.stop();  
        rotateAngle = 0;
        
        if(index_for_msg==msg.length-1){
        	index_for_msg =0;
        }
        else{
        	index_for_msg++;
        }
        	
      }
     
     gl.glEnd(); // of the text

     // ----- Render the Color Cube -----
      gl.glLoadIdentity();                // reset the current model-view matrix
      gl.glTranslatef(1.6f, 0.0f, -7.0f); // translate right and into the screen
      gl.glRotatef(angleCube, 1.0f, 1.0f, 1.0f); // rotate about the x, y and z-axes
 
      gl.glBegin(GL_QUADS); // of the color cube
 
      // Top-face
      gl.glColor3f(0.0f, 1.0f, 0.0f); // green
      gl.glVertex3f(1.0f, 1.0f, -1.0f);
      gl.glVertex3f(-1.0f, 1.0f, -1.0f);
      gl.glVertex3f(-1.0f, 1.0f, 1.0f);
      gl.glVertex3f(1.0f, 1.0f, 1.0f);
 
      // Bottom-face
      gl.glColor3f(1.0f, 0.5f, 0.0f); // orange
      gl.glVertex3f(1.0f, -1.0f, 1.0f);
      gl.glVertex3f(-1.0f, -1.0f, 1.0f);
      gl.glVertex3f(-1.0f, -1.0f, -1.0f);
      gl.glVertex3f(1.0f, -1.0f, -1.0f);
 
      // Front-face
      gl.glColor3f(1.0f, 0.0f, 0.0f); // red
      gl.glVertex3f(1.0f, 1.0f, 1.0f);
      gl.glVertex3f(-1.0f, 1.0f, 1.0f);
      gl.glVertex3f(-1.0f, -1.0f, 1.0f);
      gl.glVertex3f(1.0f, -1.0f, 1.0f);
 
      // Back-face
      gl.glColor3f(1.0f, 1.0f, 0.0f); // yellow
      gl.glVertex3f(1.0f, -1.0f, -1.0f);
      gl.glVertex3f(-1.0f, -1.0f, -1.0f);
      gl.glVertex3f(-1.0f, 1.0f, -1.0f);
      gl.glVertex3f(1.0f, 1.0f, -1.0f);
 
      // Left-face
      gl.glColor3f(0.0f, 0.0f, 1.0f); // blue
      gl.glVertex3f(-1.0f, 1.0f, 1.0f);
      gl.glVertex3f(-1.0f, 1.0f, -1.0f);
      gl.glVertex3f(-1.0f, -1.0f, -1.0f);
      gl.glVertex3f(-1.0f, -1.0f, 1.0f);
 
      // Right-face
      gl.glColor3f(1.0f, 0.0f, 1.0f); // magenta
      gl.glVertex3f(1.0f, 1.0f, -1.0f);
      gl.glVertex3f(1.0f, 1.0f, 1.0f);
      gl.glVertex3f(1.0f, -1.0f, 1.0f);
      gl.glVertex3f(1.0f, -1.0f, -1.0f);
 
      gl.glEnd(); // of the color cube
 
      // Update the rotational angle after each refresh.
      anglePyramid += speedPyramid;
      angleCube += speedCube;
   }
 
   /**
    * Called back before the OpenGL context is destroyed. Release resource such as buffers.
    * 
    */
   @Override
   public void dispose(GLAutoDrawable drawable) { }
}